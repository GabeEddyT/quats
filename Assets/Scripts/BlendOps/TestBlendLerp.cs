﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBlendLerp : TestBlend
{
    [SerializeField]
    Transform pose0, pose1;

    [SerializeField]
    [Range(0, 1)]
    float parameter = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // translation: literal linear interpolation
        pose_result.localPosition = Vector3.Lerp(pose0.localPosition, pose1.localPosition, parameter);
        // scale: literal linear interpolation
        pose_result.localScale = Vector3.Lerp(pose0.localScale, pose1.localScale, parameter);

        // rotation: SLERP if Quaternions || regular LERP
        if (usingQuaternionRotation)
        {
            pose_result.localRotation = Quaternion.Slerp(pose0.localRotation, pose1.localRotation, parameter);
        }
        else
        {
            pose_result.localEulerAngles = Vector3.Lerp(pose0.localEulerAngles, pose1.localEulerAngles, parameter);
        }
    }
}
