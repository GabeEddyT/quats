﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBlendAdd : TestBlend
{
    [SerializeField]
    Transform pose0, pose1;    

    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // translation: literal add
        pose_result.Translate(pose0.localPosition + pose1.localPosition, Space.World);

        // scale: multiply
        pose_result.localScale = pose0.localScale;
        pose_result.localScale.Scale(pose1.localScale);

        //rotation: quaternion concatenation OR Euler addition
        if (usingQuaternionRotation)
        {
            pose_result.localRotation = pose0.localRotation * pose1.localRotation;
        }
        else
        {
            pose_result.localEulerAngles = pose0.localEulerAngles + pose1.localEulerAngles;
        }
    }
}
