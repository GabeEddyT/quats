﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    // Debug 
    public Quaternion myRotation;
    public Vector3 myEuler;
    public Vector3 myTransformEuler;
    public Vector3 up;
    public Vector3 forward;
    public Vector3 right;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        myRotation = transform.rotation;
        up = transform.up;
        forward = transform.forward;
        right = transform.right;
        myEuler = transform.eulerAngles;
        myTransformEuler = transform.rotation.eulerAngles;
        
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {            
            StartCoroutine(Rot(transform.TransformDirection(Vector3.forward), transform.TransformDirection(Vector3.down)));
            //StartCoroutine(Rot(90 * transform.right));
        }
        //else if(Input.GetKeyDown(KeyCode.I))
        //{
        //    StartCoroutine(Rot(90 * Vector3.left));

        //}
        //else if(Input.GetKeyDown(KeyCode.A))
        //{            
        //    StartCoroutine(Rot(new Vector3(180f, 0, 0)));
        //}
        //else if(Input.GetKeyDown(KeyCode.S))
        //{
        //    StartCoroutine(Rot(new Vector3(270f, 0, 0)));
        //}
        else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            StartCoroutine(Rot(transform.TransformDirection(Vector3.back), transform.TransformDirection(Vector3.up)));
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            StartCoroutine(Rot(transform.TransformDirection(Vector3.left), transform.TransformDirection(Vector3.forward)));
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            StartCoroutine(Rot(transform.TransformDirection(Vector3.right), transform.TransformDirection(Vector3.forward)));
        }
    }
    [SerializeField]
    bool mutex = false; // prevent concurrent coroutines

    IEnumerator Rot(Vector3 up, Vector3 forward, Vector3? angleP = null)
    {
        Vector3 angle = angleP ?? new Vector3(90, 0, 0);
        if (mutex) yield break;
        mutex = true;
        var current = transform.rotation;
        // Check if x is secretly 180
        bool check = Mathf.Abs(Mathf.Abs(current.x) - 1) < .1f; // Check if rotation.x = 1 | -1; using .1 espilon value
        var currentX = check ? 180 : current.eulerAngles.x; // Use actual x value
        var currentY = check ? 0 : current.eulerAngles.y;
        var currentZ = check ? 0 : current.eulerAngles.z;

        Vector3 transformedVec = transform.TransformDirection(Vector3.right);
        Vector3 transformedUp = transform.TransformDirection(Vector3.forward);
        Vector3 transformedForward = transform.TransformDirection(Vector3.down);
        for (float i = 0; i < 1; i += Time.deltaTime)
        {
            //transform.rotation = Quaternion.Euler(Vector3.Lerp(new Vector3(currentX, currentY, currentZ), new Vector3(angle.x + currentX, angle.y + currentY, angle.z + currentZ), i));
            //transform.rotation = Quaternion.Slerp(current, Quaternion.AngleAxis(90 + currentX, transformedVec), i);
            transform.rotation = Quaternion.Slerp(current, Quaternion.LookRotation(forward, up), i);

            yield return null;
        }
        //transform.rotation = Quaternion.Euler(new Vector3(angle.x + currentX, angle.y + currentY, angle.z + currentZ)); // set to target angle        
        //transform.rotation = Quaternion.AngleAxis(90 + currentX, transformedVec);
        transform.rotation = Quaternion.LookRotation(forward, up);


        mutex = false;
    }
}
